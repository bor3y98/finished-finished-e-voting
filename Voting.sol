pragma solidity ^0.4.11;
// specifies what version of compiler this code will be compiled with
contract Voting {
    /* the mapping field below is equivalent to an associative array or hash.
  */

    mapping(bytes32 => uint8) public votesReceived;

    /* Solidity doesn't let you pass in an array of strings in the constructor (yet).
  We will use an array of bytes32 instead to store the list of candidates
  */

    bytes32[] public candidateList;
    mapping(address => bool) public voters;

    /* This is the constructor which will be called once and only once - when you
  deploy the contract to the blockchain. When we deploy the contract,
  we will pass an array of candidates who will be contesting in the election
  */

    function Voting(bytes32[] memory candidateNames) {
        candidateList = candidateNames;
    }

    // This function returns the total votes a candidate has received so far
    function totalVotesFor(bytes32 candidate) returns (uint8) {
        assert(validCandidate(candidate) == true);
        return votesReceived[candidate];
    }
    function validateVoter(bytes32 voterAddress) returns (bool) {
        assert(!voters[msg.sender]);
        return true;
    }

    // This function increments the vote count for the specified candidate. This
    // is equivalent to casting a vote
    function voteForCandidate(bytes32 candidate) {
        assert(validCandidate(candidate) == true);
        assert(!voters[msg.sender]);
        voters[msg.sender] = true;
        votesReceived[candidate] += 1;
    }
   function voteForMultibleCandidates(bytes32[] memory candidateNames) {
        for (uint256 i = 0; i < candidateNames.length; i++) {
        votesReceived[candidateNames[i]] += 1;
        }
        voters[msg.sender] = true;
    }

    function validCandidate(bytes32 candidate) returns (bool) {
        for (uint256 i = 0; i < candidateList.length; i++) {
            if (candidateList[i] == candidate) {
                return true;
            }
        }
        return false;
    }

}
