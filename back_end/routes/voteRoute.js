// const express = require('express');
const path = require("path");
router = require('express').Router(),
  Vote = require('../models/voteModel'),
  voteController = require('../controllers/voteController');

router.post('/create', voteController.createVote);
router.get('/', voteController.getVotes);
router.post('/cast', voteController.castVote);
router.post('/vote-details', voteController.voteDetails);
router.post('/my-votes', voteController.myVotes);
router.post('/my-votes-voted', voteController.myVotesVoted);
router.post('/result', voteController.voteResultDetails);
router.post('/stop', voteController.stopVote);





module.exports = router;