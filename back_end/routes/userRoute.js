const path = require("path");
const router = require('express').Router();
const userController = require('../controllers/userController');


router.post('/create',userController.createUser);
router.post('/sign-in',userController.signIn);
router.post('/reset-password-email',userController.resetPasswordEmail);
router.post('/reset-password',userController.resetPassword);
router.post('/reset-password-check-expire',userController.resetPasswordCheckExpire);
router.post('/edit',userController.editProfile);
router.post('/get-user-data',userController.getUserData);
router.post('/change-password',userController.changePassword);






module.exports = router;