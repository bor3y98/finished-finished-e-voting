const userModel = require("../models/userModel"),
  Web3 = require("web3"),
  web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")),
  bcrypt = require('bcrypt'),
  nodemailer = require('nodemailer'),
  jwt = require('jsonwebtoken');




exports.createUser = async (req, res, next) => {
  const userData = req.body; // from front end
  const usersLength = await userModel.count();
  let user;
  if (usersLength < 10) {
    user = await userModel.findOne({
      email: userData['email']
    });
    if (!user) {
      bcrypt.hash(userData['password'], 10, (err, hashedPassword) => {
        user = new userModel({
          email: req.body.email,
          fullName: req.body.name,
          address: web3.eth.accounts[usersLength], //validate
          password: hashedPassword
        });
        user.save().then(savedUser => {
          let token = jwt.sign({
            fullName: savedUser.fullName,
            email: savedUser.email,
            address: savedUser.address
          }, 'secret text');
          savedUser.token = token;
          savedUser.save().then(lastUser => {
            res.status(200).json({
              'token': lastUser.token
            })
          })
        })
      });
    } else {
      res.status(200).json({
        isError: true,
        message: 'This email is already registered!'
      })
    }
  } else {
    res.status(200).json({
      isError: true,
      message: 'Maximum number of users are 10'
    })
  }
}
exports.resetPasswordEmail = async (req, res, next) => {
  let user = await userModel.findOne({
    email: req.body.email
  });
  if (user) {
    let ts = Date.now();

    let date_ob = new Date(ts);
    let token = jwt.sign({
      email: req.body.email,
      date: date_ob
    }, 'secret text', {
      expiresIn: '1h'
    });
    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'pollzevoting@gmail.com',
        pass: 'Abdo12345678'
      }
    });
    var mailOptions = {
      from: 'pollzevoting@gmail.com',
      to: req.body.email,
      subject: 'Pollz Reset Password',
      text: 'Your reset password email is :' + 'http://localhost:4200/password-reset/' + token
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  } else {
    res.status(200).json({
      isError: true,
    })
  }

}
exports.resetPassword = async (req, res, next) => {
  try {
    let data = await jwt.verify(req.body['data'].token, 'secret text');
    let user = await userModel.findOne({
      email: data['email']
    });
    if (user) {
      bcrypt.hash(req.body['data']['password'], 10, (err, hashedPassword) => {
        user.password = hashedPassword;
        user.save().then(() => {
          res.status(200).json({
            isError: false
          });
        })
      });
    } else {
      res.status(200).json({
        isError: true,
        message: "This user doesn't exists"
      });
    }

  } catch (error) {
    console.log(error)
    if (error['name'] === 'TokenExpiredError') {
      res.status(200).json({
        isError: true,
        expired: true
      })
    } else {
      res.status(200).json({
        isError: true,
      })
    }
  }
}
exports.resetPasswordCheckExpire = async (req, res, next) => {
  try {
    let data = await jwt.verify(req.body.token, 'secret text');
    res.status(200).json({
      isError: false
    })

  } catch (error) {
    if (error['name'] === 'TokenExpiredError') {
      res.status(200).json({
        isError: true,
        expired: true
      })
    } else {
      res.status(200).json({
        isError: true,
      })
    }
  }
}



exports.signIn = async (req, res, next) => {
  const userData = req.body; // from front end
  let user = await userModel.findOne({
    email: userData['email'],
  });
  if (user) {
    bcrypt.compare(userData['password'], user.password, function (err, response) {
      if (response) {
        res.status(200).json({
          'token': user.token,
          'fullName': user.fullName
        });
      } else {
        res.status(200).json({
          isError: true,
          message: 'Wrong email or password'
        });
      }
    });
  } else {
    res.status(200).json({
      isError: true,
      message: 'Wrong email or password'
    });
  }
}



exports.changePassword = async (req, res, next) => {
  const data = req.body; // from front end
  let user = await userModel.findOne({
    email: data['email'],
  });
  if (user) {
    bcrypt.compare(data['oldPassword'], user.password, function (err, response) {
      if (response) {
        bcrypt.hash(data['newPassword'], 10, (err, hashedPassword) => {
          user.password = hashedPassword
          user.save().then(() => {
            res.status(200).json({
              'isError': false
            })
          })
        })
      }else{
        
        res.status(200).json({
          isError:true,
          message:'Your old password is wrong'
        })
      }
    })
  }
}
// edit profile .....
exports.editProfile = async (req, res, next) => {
  console.log('ss')
  let user = await userModel.findOne({
    token: req.body.token,
  });

  user.fullName = req.body.data.name;
  user.email = req.body.data.email;

  user.save().then(savedUser => {
    let token = jwt.sign({
      fullName: savedUser.fullName,
      email: savedUser.email,
      address: savedUser.address
    }, 'secret text');
    savedUser.token = token;
    savedUser.save().then(lastUser => {
      res.status(200).json({
        'token': lastUser.token,
        'fullName': lastUser.fullName

      })
    })
  })

}

exports.getUserData = async (req, res, next) => {
  let data = await jwt.verify(req.body.token, 'secret text');

  let user = await userModel.findOne({
    email: data['email'],
  });

  res.status(200).json({
    'email': user.email,
    'name': user.fullName
  });

}






function createNowDate() {
  let ts = Date.now();

  let date_ob = new Date(ts);
  let date = date_ob.getDate();
  let month = date_ob.getMonth() + 1;
  let year = date_ob.getFullYear();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes();
  let seconds = date_ob.getSeconds();

  // prints date & time in YYYY-MM-DD format
  return year + "-" + month + "-" + date + "-" + hours + "-" + minutes + "-" + seconds;
}

function getExpireDate(endDate) {
  var nowDate = new Date();
  var date = nowDate.getFullYear() + '-' + (nowDate.getMonth() + 1) + '-' + nowDate.getDate();
  let currentDate;
  currentDate = date.split('-')
  endDate = endDate.split('-');
  const months = endDate[1] - currentDate[1];
  const days = endDate[0] - currentDate[2];
  const years = endDate[2] - currentDate[0];
  let returnMessage = '';
  if (years > 0) {
    returnMessage = true;
  } else if (months > 0 && years === 0) {
    returnMessage = true;
  } else if (days > 0 && months === 0) {
    returnMessage = true
  }

  return returnMessage;
}


/*


 
 */