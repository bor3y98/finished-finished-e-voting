const voteModel = require("../models/voteModel"),
  userModel = require('../models/userModel'),
  userVoteModel = require('../models/userVoteModel.js'),
  jwt = require('jsonwebtoken'),
  Web3 = require("web3"),
  slugify = require('slugify'),
  base64Img = require('base64-img'),
  crypto = require('crypto'),
  ObjectId = require('mongodb').ObjectId,
  web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")),
  solc = require("solc"), // import the solidity compiler
  fs = require("fs"), //import the file system module
  code = fs.readFileSync("Voting.sol").toString();
const compiledCode = solc.compile(code),
  byteCode = compiledCode.contracts[":Voting"].bytecode,
  abi = compiledCode.contracts[":Voting"].interface,
  abiDefinition = JSON.parse(abi),
  VotingContract = web3.eth.contract(abiDefinition);


exports.createVote = async (req, res, next) => {
  const data = req.body['data'];
  let privateLink = '';
  let images = [];
  if (data['optionsImages']) {
    for (let i = 0; i < data['optionsImages'].length; i++) {
      if (data['optionsImages'][i] && data['optionsImages'][i]['_pendingValue'].length > 0) {
        images.push(convertImageBase64(data['title'], data['optionsImages'][i], i));
      }
      else {
        images.push("NONE");
      }
    }
  }
  let userData = await jwt.verify(req.body['token'], 'secret text');
  let address = userData['address']
  var deployedContract = VotingContract.new(
    data['options'], {
    data: byteCode,
    from: address,
    gas: 4700000
  },
    async function (e, contract) {
      if (!e) {
        if (!contract.address) {
          console.log("Contract transaction send: TransactionHash: " + contract.transactionHash + " waiting to be mined...\n");
        } else {
          console.log("Contract mined! Address: " + contract.address);
          if (!data['isPublic']) {
            privateLink = await crypto.createHash('md5').update(data['title'] + createNowDate() + userData['email']).digest('hex');
          }
          let vote = voteModel({
            title: data['title'],
            user: userData['email'],
            optionImages: images,
            options: data['options'],
            startDate: data['startDate'],
            endDate: data['endDate'],
            isPrivate: !data['isPublic'],
            isMulti: data['isMulti'],
            address: contract.address,
            privateLink: privateLink
          });
          await vote.save().then(lastVote => {
            if (!data['isPublic']) {
              privateLink = 'http://localhost:4200/pvote/cast/' + lastVote.privateLink
            }

            res.status(200).json({
              isError: false,
              privateLink
            })
          })

        }
      } else {
        console.log(e)
        res.status(200).json({
          isError: true
        })
      }
    }
  );
}






function getExpireDate(endDate) {
  var nowDate = new Date();
  var date = nowDate.getFullYear() + '-' + (nowDate.getMonth() + 1) + '-' + nowDate.getDate();
  let currentDate;
  currentDate = date.split('-')
  endDate = endDate.split('-');
  const months = endDate[1] - currentDate[1];
  const days = endDate[0] - currentDate[2];
  const years = endDate[2] - currentDate[0];
  let returnMessage = '';
  if (years > 0) {
    returnMessage = true;
  } else if (months > 0 && years === 0) {
    returnMessage = true;
  } else if (days >= 0 && months === 0) {
    returnMessage = true
  }

  return returnMessage;
}



exports.getVotes = async (req, res, next) => {
  let votesArr = [];
  let voteUsers = [];
  try {
    let votes = await voteModel.find({
      isPrivate: false
    });
    for (let i in votes) {
      if (getExpireDate(votes[i].endDate)) {
        votesArr.push(votes[i]);
        let user = await userModel.findOne({ email: votes[i].user })
        if (user) {
          voteUsers.push(user.fullName);
        }
      }
    }
    res.status(200).json({
      votesArr,
      voteUsers
    })
  } catch (e) {
    console.log(e)
    res.status(200).json({
      isError: true
    })
  }
}



exports.castVote = async (req, res, next) => {
  const bodyData = req.body['data']
  let userData = await jwt.verify(req.body['token'], 'secret text');
  address = userData['address']
  let currentVote
  try {
    var o_id = new ObjectId(bodyData['id']);
    currentVote = await voteModel.findOne({ _id: o_id });
  }
  catch{
    currentVote = await voteModel.findOne({ privateLink: bodyData['id'] });
  }
  contractInstance = VotingContract.at(currentVote['address']);
  let userVote = await userVoteModel.findOne({ vote: currentVote, user: userData['email'] })
  if (userVote) {
    res.status(200).json({
      isError: true,
      message: 'You already voted for this poll!'
    });
  }
  else {
    console.log(bodyData)
    if (currentVote.isMulti) {
      try {
        contractInstance.voteForMultibleCandidates(bodyData['option'], {
          from: address
        });
        // contractInstance.validateVoter.call(address).valueOf();
        let userVote = userVoteModel({
          user: userData['email'],
          vote: currentVote
        })
        userVote.save();
        res.status(200).json({
          message: 'Vote was casted successufly!'
        });
      } catch (e) {
        console.log(e)
        res.status(200).json({
          isError: true,
          message: 'This user already voted for this vote!'
        });
      }
    }
    else {
      try {
        contractInstance.voteForCandidate(bodyData['option'], {
          from: address
        });
        let userVote = userVoteModel({
          user: userData['email'],
          vote: currentVote
        })
        userVote.save();
        res.status(200).json({
          message: 'Vote was casted successufly!'
        });
      } catch (e) {
        console.log('-----------------------------', e)
        res.status(200).json({
          isError: true,
          message: 'This user already voted for this vote!'
        });
      }
    }
  }
}
// Vote details for cast vote pages
exports.voteDetails = async (req, res, next) => {
  try {
    var o_id = new ObjectId(req.body['id']);
    let vote = await voteModel.findOne({ _id: o_id });
    if (vote) {
      let userData = await jwt.verify(req.body['token'], 'secret text');
      let userVote = await userVoteModel.findOne({ vote: vote, user: userData['email'] })
      if (getExpireDate(vote.endDate) && !vote.isPrivate) {
        if (userVote) {
          res.status(200).json({
            isVoted: true

          });
        }
        else {
          res.status(200).json({
            vote
          });
        }
      }
      else {
        res.status(200).json({
          isError: true,
          message: "This vote doesn't exist"
        })
      }
    }
    else {

      res.status(200).json({
        isError: true,
        message: "This vote doesn't exist"
      })
    }
  }
  catch (e) {

    let vote = await voteModel.findOne({ privateLink: req.body['id'] });
    if (vote) {
      let userData = await jwt.verify(req.body['token'], 'secret text');
      let userVote = await userVoteModel.findOne({ vote: vote, user: userData['email'] })
      if (getExpireDate(vote.endDate)) {
        if (userVote) {
          res.status(200).json({
            isVoted: true

          });
        }
        else {
          res.status(200).json({
            vote
          });
        }
      }
      else {
        res.status(200).json({
          isError: true,
          message: "This vote doesn't exist"
        })
      }
    }
    else {

      res.status(200).json({
        isError: true,
        message: "This vote doesn't exist"
      })
    }
  }
}

exports.voteResultDetails = async (req, res, next) => {
  try {
    var o_id = new ObjectId(req.body['id']);
    let vote = await voteModel.findOne({ _id: o_id });
    let userData = await jwt.verify(req.body['token'], 'secret text');
    let userVote = await userVoteModel.findOne({ vote: vote, user: userData['email'] })
    const voteOwner = (vote.user === userData.email)
    if (userVote || voteOwner) {
      if (!vote.isPrivate) {
        let voteOptions = {};
        contractInstance = VotingContract.at(vote['address']);
        for (let i = 0; i < vote['options'].length; i++) {
          voteOptions[vote['options'][i]] = contractInstance.totalVotesFor.call(vote.options[i]).valueOf()
        }
        res.status(200).json({
          vote,
          voteOptions,
          voteOwner
        });
      }
    } else {
      res.status(404).json({
        isVoted: false
      });
    }
  }
  catch{
    let vote = await voteModel.findOne({ privateLink: req.body['id'] });
    if (vote) {

      let userData = await jwt.verify(req.body['token'], 'secret text');
      let userVote = await userVoteModel.findOne({ vote: vote, user: userData['email'] })
      const voteOwner = (vote.user === userData.email)
      if (userVote || voteOwner) {
        let voteOptions = {};
        contractInstance = VotingContract.at(vote['address']);
        for (let i = 0; i < vote['options'].length; i++) {
          voteOptions[vote['options'][i]] = contractInstance.totalVotesFor.call(vote.options[i]).valueOf()
        }
        res.status(200).json({
          vote,
          voteOptions,
          voteOwner
        });
      } else {
        res.status(404).json({
          isVoted: false
        });
      }
    }
    else {
      res.status(404);
    }
  }
}
exports.stopVote = async (req, res, next) => {
  console.log('--------------------------------')
  var o_id = new ObjectId(req.body['id']);
  let vote = await voteModel.findOne({ _id: o_id });
  let userData = await jwt.verify(req.body['token'], 'secret text');
  if (userData.email === vote.user) {
    // 24-04-2020
    let ts = Date.now();
    let date_ob = new Date(ts);
    let date = date_ob.getDate() - 1;
    let month = date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();
    vote.endDate = date + '-' + month + '-' + year;
    console.log(vote.endDate)
    vote.save().then(savedVote => {
      res.status(200).json({
        savedVote
      })
    });

  }
}
// Votes i voted in
exports.myVotesVoted = async (req, res, next) => {
  let userData = await jwt.verify(req.body['token'], 'secret text');
  let userVotes = await userVoteModel.find({ user: userData['email'] });
  let votes = []
  for (let i = 0; i < userVotes.length; i++) {

    var o_id = new ObjectId(userVotes[i].vote);
    let currentVote = await voteModel.findOne({ _id: o_id });
    if (currentVote) {
      votes.push(currentVote)
    }
  }
  // userVote.forEach(async vote => {
  // })
  if (votes) {
    res.status(200).json({
      votes
    });
  }
  else {
    res.status(200).json({
      isError: true,
      message: "There is not created votes yet"
    })
  }
}
exports.myVotes = async (req, res, next) => {
  let userData = await jwt.verify(req.body['token'], 'secret text');
  let votes = await voteModel.find({ user: userData['email'] });
  if (votes) {
    res.status(200).json({
      votes
    });
  }
  else {
    res.status(200).json({
      isError: true,
      message: "There is not created votes yet"
    })
  }
}
function convertImageBase64(voteName, base64Image, index) {
  if (base64Image['_pendingValue'].length > 0) {
    voteName = slugify(voteName);
    console.log(voteName)
    const imageName = slugify(voteName + index + '-' + createNowDate());
    const imagePath = base64Img.imgSync(
      base64Image['_pendingValue'], `./back_end/img/${voteName}`, imageName
    );

    return imagePath.substring(13)

  }
  else {
    return 'NONE'
  }
}

function createNowDate() {
  let ts = Date.now();

  let date_ob = new Date(ts);
  let date = date_ob.getDate();
  let month = date_ob.getMonth() + 1;
  let year = date_ob.getFullYear();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes();
  let seconds = date_ob.getSeconds();

  // prints date & time in YYYY-MM-DD format
  return year + "-" + month + "-" + date + "-" + hours + "-" + minutes + "-" + seconds;
}