const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");


const vote = mongoose.Schema({
  title: { type: String },
  user: { type: String }, //vote creator
  address: { type: String },
  optionImages: [],
  options: [], // requried
  startDate: { type: String },
  endDate: { type: String },
  isPrivate: { type: Boolean, default: false },
  isMulti: { type: Boolean, default: false },
  privateLink: { type: String }
});



// voteOptionSchema.plugin(uniqueValidator);
module.exports = mongoose.model("vote", vote);
// module.exports = mongoose.model("userVote", userVote);

