const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");


const userVote = mongoose.Schema({
    vote: { type: mongoose.ObjectId },
    user: { type: String }
});


module.exports = mongoose.model("userVote", userVote);

