const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");


const user = mongoose.Schema({
    fullName:{type:String},
    email:{type:String , required: true ,unique: true},
    address:{type:String},  //PK
    password:{type:String},
    token:{type:String ,unique: true},
    
});

user.plugin(uniqueValidator);
module.exports = mongoose.model("user", user);
