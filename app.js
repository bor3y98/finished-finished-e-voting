const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const voteRoute = require("./back_end/routes/voteRoute");
const userRoute = require("./back_end/routes/userRoute");


const app = express();
publicDir = require("path").join(__dirname, "./back_end/img");
app.use(express.static(publicDir));

mongoose
  .connect(
    //database name
    "mongodb://localhost/eVote", {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
  )
  .then(() => {
    console.log("Connected to database!");
  })
  .catch(() => {
    console.log("Connection failed!");
  });

app.use(bodyParser.json({ limit: '10000mb' }));
app.use(bodyParser.urlencoded({ extended: false }));



app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});
app.use("/vote", voteRoute);
app.use("/user", userRoute);


module.exports = app;